const express = require("express")
const app = express()
const bodyParser = require("body-parser")
const questionsRouter = require("./routes/questions")
const answersRouter = require("./routes/answers")

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: false
}))

app.use(function (req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*")
    res.setHeader("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE")
    if (req.header("Access-Control-Request-Headers"))
        res.setHeader("Access-Control-Allow-Headers", req.header("Access-Control-Request-Headers"))
    next()
})

app.use("/questions", questionsRouter)
app.use("/answers", answersRouter)

app.set('port', (process.env.PORT || 3000));
app.listen(app.get('port'), function () {
    console.log('Node server is running on port ' + app.get('port'));
});