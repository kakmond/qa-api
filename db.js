const mysql = require('mysql');
const db = mysql.createConnection({
  host: 'db4free.net',
  user: 'question_answer_',
  password: '9ff119f1',
  database: 'question_answer_',
});

db.connect();

db.query(`
	CREATE TABLE IF NOT EXISTS questions (
		id INTEGER AUTO_INCREMENT PRIMARY KEY,
		author TEXT,
        description TEXT,
		createdAt BIGINT
	)
`)

db.query(`
	CREATE TABLE IF NOT EXISTS answers (
		id INTEGER AUTO_INCREMENT PRIMARY KEY,
        questionId INTEGER,
        author TEXT,
        description TEXT,
        createdAt BIGINT,
        FOREIGN KEY (questionId) REFERENCES questions(id)
	)
`)



exports.getAllQuestions = function (callback) {
    const query = `
    SELECT
        x.*
    FROM (
        SELECT
            q.id , q.author, q.createdAt, q.description, COUNT(a.id) AS "answerCount"
        FROM
            questions q
        LEFT JOIN 
            answers a ON q.id = a.questionId
        GROUP BY
            q.id
    ) x 
    ORDER BY x.createdAt desc
    `
    const values = []
    db.query(query, values, function (error, questions) {
        callback(error, questions)
    })
}

exports.getQuestionById = function (id, callback) {
    const query = `
    SELECT
        x.*
    FROM (
        SELECT
            q.id , q.author, q.createdAt, q.description, COUNT(a.id) AS "answerCount"
        FROM
            questions q
        LEFT JOIN 
            answers a ON q.id = a.questionId
        GROUP BY
            q.id
    ) x 
    WHERE 
        x.id = ?
    ORDER BY x.createdAt desc
    `
    const values = [id]
    db.query(query, values, function (error, question) {
        callback(error, question[0])
    })
}


exports.createQuestion = function (question, callback) {
    const query = `
        INSERT INTO questions 
            (author, description, createdAt) 
        VALUES 
            (?, ?, ?)
    `
    const values = [
        question.author,
        question.description,
        question.createdAt
    ]
    db.query(query, values, function (error) {
        const id = this.lastID
        callback(error, id)
    })
}

exports.createAnswer = function (answer, callback) {
    const query = `
        INSERT INTO answers 
            (questionId, author, description, createdAt) 
        VALUES 
            (?, ?, ?, ?)
    `
    const values = [
        answer.questionId,
        answer.author,
        answer.description,
        answer.createdAt
    ]
    db.query(query, values, function (error) {
        const id = this.lastID
        callback(error, id)
    })
}

exports.getAnswersByQuestionId = function (id, callback) {
    const query = `
        SELECT 
            a.*
        FROM 
            answers a
        WHERE 
            a.questionId = ?
        ORDER BY createdAt desc
    `
    const values = [id]
    db.query(query, values, function (error, question) {
        callback(error, question)
    })
}

exports.getAnswerById = function (id, callback) {
    const query = `
    SELECT 
        a.*
    FROM 
        answers a
    WHERE 
        a.id = ?
    ORDER BY createdAt desc
    `
    const values = [id]
    db.query(query, values, function (error, answer) {
        callback(error, answer[0])
    })
}