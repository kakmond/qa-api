const express = require("express")
const db = require("../db")
const app = express.Router()
const AUTHOR_MIN_LENGTH = 4
const AUTHOR_MAX_LENGTH = 10
const ANSWER_MIN_LENGTH = 4
const ANSWER_MAX_LENGTH = 100

app.post("/", function (req, res) {
    const validationErrors = []
    const questionId = req.body.questionId
    const author = req.body.author
    const description = req.body.description

    if (!author)
        validationErrors.push("author is required")
    else if (author.length < AUTHOR_MIN_LENGTH)
        validationErrors.push("author is too short")
    else if (author.length > AUTHOR_MAX_LENGTH)
        validationErrors.push("author is too long")
    if (!description)
        validationErrors.push("description is required")
    else if (description.length < ANSWER_MIN_LENGTH)
        validationErrors.push("description is too short")
    else if (description.length > ANSWER_MAX_LENGTH)
        validationErrors.push("description is too long")
    if (validationErrors.length > 0) {
        res.status(400).json(validationErrors)
        return
    }
    const answer = {
        questionId,
        author,
        description,
        createdAt: new Date().getTime()
    }
    db.createAnswer(answer, function (error, id) {
        if (error)
            if (error.errno == 1452)
                res.status(400).json(["question doesn't exist"])
            else {
                console.log(error)
                res.status(500).end()
            }
        else 
            res.status(201).end()
    })
})

app.get("/:id", function (req, res) {
    const id = req.params.id
    db.getAnswerById(id, function (error, answer) {
        if (error) {
            console.log(error)
            res.status(500).end()
        }
        else
            if (answer)
                res.status(200).json(answer)
            else
                res.status(404).end()
    })
})

module.exports = app