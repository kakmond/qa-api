const express = require("express")
const db = require("../db")
const app = express.Router()
const AUTHOR_MIN_LENGTH = 4
const AUTHOR_MAX_LENGTH = 10
const DESCRIPTION_MIN_LENGTH = 10
const DESCRIPTION_MAX_LENGTH = 100

app.get("/", function (req, res) {
    db.getAllQuestions(function (error, questions) {
        if (error) {
            console.log(error)
            res.status(500).end()
        } else
            res.status(200).json(questions)
    })
})

app.post("/", function (req, res) {
    const validationErrors = []
    const author = req.body.author
    const description = req.body.description
    if (!author)
        validationErrors.push("author is required")
    else if (author.length < AUTHOR_MIN_LENGTH)
        validationErrors.push("author is too short")
    else if (author.length > AUTHOR_MAX_LENGTH)
        validationErrors.push("author is too long")
    if (!description)
        validationErrors.push("description is required")
    else if (description.length < DESCRIPTION_MIN_LENGTH)
        validationErrors.push("description is too Short")
    else if (description.length > DESCRIPTION_MAX_LENGTH)
        validationErrors.push("description is too long")
    if (validationErrors.length > 0) {
        res.status(400).json(validationErrors)
        return
    }
    const question = {
        author,
        description,
        createdAt: new Date().getTime()
    }
    db.createQuestion(question, function (error, id) {
        if (error) {
            console.log(error)
            res.status(500).end()
        }
        else 
            res.status(201).end()
    })
})

app.get("/:id", function (req, res) {
    const id = req.params.id
    db.getQuestionById(id, function (error, question) {
        if (error) {
            console.log(error)
            res.status(500).end()
        }
        else
            if (question)
                res.status(200).json(question)
            else
                res.status(404).end()
    })
})

app.get("/:id/answers", function (req, res) {
    const id = req.params.id
    db.getAnswersByQuestionId(id, function (error, answer) {
        if (error) {
            console.log(error)
            res.status(500).end()
        }
        else
            res.status(200).json(answer)
    })
})

module.exports = app